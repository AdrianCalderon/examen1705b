@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de Ingredient
</h1>

<a href="/ingredients/create">
Alta de ingrediente
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Tipo Ingrediente</th>
    </tr>


@foreach ($ingredients as $ingredient)
    <tr>
        <td>{{ $ingredient->id }}</td>
        <td>{{ $ingredient->name }}</td>
        <td>{{ $ingredient->type->name }}</td>
        
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $ingredients->links() }}
</div>
@endsection