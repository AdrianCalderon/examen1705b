<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Ingredient::class, function () {
    $faker = Faker\Factory::create('es_ES');

    return [
        'name' => 'Ingrediente ' . str_random(6),
        // 'description' => $faker->paragraph(),
        // 'stock' => rand(3, 100),
        'type_id' => rand(1, 6),
    ];
});

$factory->define(App\Pizza::class, function () {
    static $password;
    $faker = Faker\Factory::create('es_ES');

    return [
        'name' => 'Pizza ' .  $faker->words(1)[0],
        // 'description' => $faker->paragraph(),
        // 'price' => rand(3, 100),
        'user_id' => rand(1, 4),
    ];
});
